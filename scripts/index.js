function getNumberInput(promptText, defaultValue) {
  let userInput = prompt(promptText, defaultValue);
  while (userInput === null || userInput === "" || isNaN(userInput)) {
    userInput = prompt("Будь ласка, введіть коректне число:", defaultValue);
  } 
  return parseFloat(userInput);
}
const firstNumber = getNumberInput("Введіть перше число:", "");
const secondNumber = getNumberInput("Введіть друге число:", "");
let operation = prompt("Введіть математичну операцію (+, -, *, /):");
while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
  operation = prompt("Будь ласка, введіть коректну операцію (+, -, *, /):");
}
function performOperation(a, b, operator) {
  switch (operator) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
    default:
      return NaN;
  }
}
const result = performOperation(firstNumber, secondNumber, operation);
console.log(`Результат: ${result}`);
